# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
    i = 1
    soma = 0

    while( i < n ) # Vamos achar a soma de todos os números de 1 até n -1
        soma = soma + i 
        i = i + 1
    end

    primeiro_numero = 2(soma + 1) - 1 
    return primeiro_numero
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)

    primeiro = impares_consecutivos(m)
    print(m, " ", m^3, " ")
    
    j = 0

    while(j < m)
        print(primeiro, " ") 
        primeiro = primeiro + 2
        j = j + 1

    end
end

function mostra_n(n)
    
    m = 1

    for m in 1:n 
        imprime_impares_consecutivos(m)
        println("")
    end

end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()
